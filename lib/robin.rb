#
# Robin is a simple, multithreaded queue runner, designed for compatibility and
# concurrency. It will eventually support AMQP, SQS, and Resque-compatible
# message brokers, with third-party adapters supported and encouraged. Robin is
# tested on MRI 2.0.0, JRuby 1.7.8, and Rubinius 2.2.1.
#
# @author David P. Kleinschmidt
#
module Robin
  autoload :Bus,       'robin/bus'
  autoload :Cli,       'robin/cli'
  autoload :Queue,     'robin/queue'
  autoload :Runner,    'robin/runner'
  autoload :VERSION,   'robin/version'
  autoload :Worker,    'robin/worker'

  #
  # Processes background jobs until interrupted or all queues have exited. To
  # interrupt the jobs, raise {::Interrupt} in its thread (if running in the
  # main thread, the user can interrupt by pressing `Control-C` or sending
  # `SIGINT` to the process). After an interrupt is received, a best effort is
  # made to finish in-progress tasks and tasks which have been dequeued but not
  # yet assigned to a worker. After the interrupt is processed, this method
  # returns normally.
  #
  # @param [Enumerable<Queue>] queues
  # @param [Enumerable<Worker>] workers
  # @return [void]
  #
  def self.run(queues, workers)
    return if queues.empty?
    if workers.empty?
      raise ArgumentError.new 'Must have workers if there are queues.'
    end

    begin
      bus = Bus.new
      worker_threads = workers.map do |worker|
        Thread.new { worker.run &bus.method(:get) }.tap {|t| t.abort_on_exception = true}
      end
      queue_threads = queues.map do |queue|
        Thread.new { queue.run &bus.method(:put) }.tap {|t| t.abort_on_exception = true}
      end
      queue_threads.each &:join
    rescue Interrupt
      queue_threads.each { |thread| thread.raise Interrupt.new }
    ensure
      workers.each { bus.put Worker::Stop }
      worker_threads.each &:join
    end
  end
end

#
# Base class for message queues.
#
# @abstract Subclasses must implement `dequeue`.
#
class Robin::Queue
  autoload :Slow, 'robin/queue/slow'

  #
  # Runs the queue indefinitely. To interrupt the queue, raise {::Interrupt} in
  # its thread.
  #
  # @return [void]
  #
  def run
    loop { yield dequeue }
  rescue Interrupt
  end
end

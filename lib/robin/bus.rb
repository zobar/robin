module Robin

  #
  # Manages message handoff from queues to workers.
  #
  class Bus

    #
    # Retrieves the next available message, blocking if none is available.
    #
    def get
      lock.synchronize do
        worker_available.wait_while { on_deck }
        @on_deck = true
        worker_on_deck.signal
        message_available.wait_until { defined? @message }
        remove_instance_variable(:@message).tap { worker_available.signal }
      end
    end

    def initialize
      @lock = Monitor.new
      @message_available = lock.new_cond
      @on_deck = false
      @worker_available = lock.new_cond
      @worker_on_deck = lock.new_cond
    end

    #
    # Submits a new message to a worker, blocking if none is available. This
    # ensures that, in the case of worker saturation, the queues do not get too
    # far ahead of the workers.
    #
    # @return [void]
    #
    def put(message)
      lock.synchronize do
        begin
          worker_on_deck.wait_until { on_deck }
          @message = message
          @on_deck = false
          message_available.signal
        rescue Interrupt
          put message
          raise
        end
      end
    end

    private

    attr_reader :lock, :message_available, :on_deck, :worker_available, :worker_on_deck
  end
end

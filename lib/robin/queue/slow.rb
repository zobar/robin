module Robin

  #
  # Dumb queue implementation that sleeps for a length of time before enqueueing
  # an equally dumb message.
  #
  class Queue::Slow < Queue

    #
    # The number of seconds this queue sleeps between messages
    #
    attr_reader :duration

    #
    # @param [Fixnum] duration The number of seconds to sleep between messages
    #
    def initialize(duration)
      @duration = duration
    end

    #
    # Sleeps {#duration} seconds, then produces a dumb message.
    #
    def dequeue
      sleep duration
      "Hello from queue #{duration}!"
    end
  end
end

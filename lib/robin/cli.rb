require 'thor'

module Robin

  #
  # Command-line interface to the robin queue runner.
  #
  class Cli < Thor

    desc 'work', 'Runs the robin workers'

    #
    # Runs the robin workers.
    #
    # @return [void]
    #
    def work
      Robin.run(
        [5, 7, 11, 13, 17].map(&Queue::Slow.method(:new)),
        (0...4).map { |i| Worker::Slow.new 12 }
      )
    end
  end
end

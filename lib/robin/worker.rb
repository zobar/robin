#
# Base class for workers.
#
# @abstract Subclasses must implement `process`.
#
class Robin::Worker
  autoload :Slow, 'robin/worker/slow'

  #
  # Singleton object that signals the worker to stop.
  #
  Stop = Object.new

  #
  # Runs the worker until it receives {Stop}.
  #
  # @return [void]
  #
  def run
    until (message = yield) == Stop
      process message
    end
  end
end

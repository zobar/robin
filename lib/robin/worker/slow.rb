module Robin

  #
  # Dumb worker implementation that sleeps for a length of time.
  #
  class Worker::Slow < Worker

    #
    # The number of seconds this worker sleeps per message.
    #
    attr_reader :duration

    #
    # @param [Fixnum] duration The number of seconds to sleep between messages
    #
    def initialize(duration)
      @duration = duration
    end

    #
    # Sleeps {#duration} seconds.
    #
    def process(message)
      sleep duration
    end
  end
end

# coding: utf-8

$:.unshift File.join(File.dirname(__FILE__), 'lib')

require 'robin/version'

def Robin.gemspec
  @gemspec ||= Gem::Specification.new do |s|
    s.authors     = ['David P Kleinschmidt']
    s.description = 'Queue runner with pluggable backends.'
    s.email       = 'david@kleinschmidt.name'
    s.homepage    = 'http://github.com/zobar/robin'
    s.license     = 'MIT'
    s.name        = 'robin'
    s.summary     = 'The queue runner that people love™'
    s.version     = Robin::VERSION

    s.executables = 'robin'
    s.files       = Dir['LICENSE.txt', 'lib/**/*.rb'].sort

    s.add_dependency 'thor'

    s.add_development_dependency 'minitest'
    s.add_development_dependency 'rake'
    s.add_development_dependency 'simplecov'
    s.add_development_dependency 'yard'
  end
end

Robin.gemspec

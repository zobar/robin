require 'spec_helper'

class CountingQueue < Robin::Queue
  attr_reader :count

  def initialize
    @count = 0
  end

  def dequeue
    @count += 1
  end
end

class InterruptingWorker < Robin::Worker
  attr_reader :count

  def initialize
    super
    @count = 0
    @thread = Thread.current
  end

  def process(message)
    @count += 1
    unless @interrupted
      @thread.raise Interrupt
      @interrupted = true
    end
  end
end

describe Robin do
  subject { Robin }

  it 'knows its version' do
    subject::VERSION.wont_be_nil
  end

  describe 'run' do
    subject { Robin.run queues, workers }

    describe 'with no queues' do
      let(:queues)  { [] }

      describe 'and no workers' do
        let(:workers) { [] }

        it 'exits immediately' do
          subject
        end
      end

      describe 'and a worker' do
        let(:workers) { [Robin::Worker.new] }

        it 'exits immediately' do
          subject
        end
      end
    end

    describe 'with a queue' do
      let(:queue)  { CountingQueue.new }
      let(:queues) { [queue] }

      describe 'and no workers' do
        let(:workers) { [] }

        it 'raises ArgumentError' do
          -> { subject }.must_raise ArgumentError
        end
      end

      describe 'and a worker' do
        let(:workers) { [InterruptingWorker.new] }

        it 'dequeues two items' do
          subject
          queue.count.must_equal 2
        end

        it 'processes two items' do
          subject
          queue.count.must_equal 2
        end
      end
    end
  end
end
